package com.OrangeHrm.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import com.OrangeHrm.utility.CommonMethod;
import com.OrangeHrm.utility.ExcelConnect;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class LoginPage {
WebDriver driver;
JavascriptExecutor js;
CommonMethod cm=new CommonMethod();
ExcelConnect ec=new ExcelConnect();
Logger logger = Logger.getLogger("LoginPage");

public LoginPage(WebDriver ldriver){
	this.driver=ldriver;

}
@FindBy(xpath="//input[@id='txtUsername']")
WebElement UserNameButton;
@FindBy(xpath="//input[@id='txtPassword']")
WebElement PasswordButton;
@FindBy(xpath="//input[@id='btnLogin']")
WebElement LoginButton;
@FindBy(xpath="//a[@id='menu_admin_viewAdminModule']")
WebElement AdministratorModule;
@FindBy(xpath="//span[@id='spanMessage']")
WebElement InvalidCredentialsLoginMessage;
@FindBy(xpath="//b[contains(.,'Admin')]")
WebElement Admin;
@FindBy(xpath="//input[@id='searchSystemUser_userName']")
WebElement UserName;
@FindBy(xpath="//select[@id='searchSystemUser_userType']")
WebElement UserType;
@FindBy(xpath="//input[@id='searchSystemUser_employeeName_empName']")
WebElement EmployeeName;
@FindBy(xpath="//input[@id='searchSystemUser_employeeName_empName']")
WebElement Status;
@FindBy(xpath="//input[@id='btnAdd']")
WebElement AddButtoninAdmin;
@FindBy(xpath="//input[@id='searchBtn']")
WebElement SearchButton;
@FindBy(xpath="//input[@id='resetBtn']")
WebElement ResetButton;
@FindBy(xpath="//select[@id='systemUser_userType']")
WebElement UserRole;
@FindBy(xpath="//select[@id='systemUser_userType']")
WebElement UserRoleDropdown;
@FindBy(xpath="//input[@id='systemUser_employeeName_empName']")
WebElement EmployeeNameinAdd;
@FindBy(xpath="//input[@id='systemUser_userName']")
WebElement UserNameinAdd;
@FindBy(xpath="//select[@id='systemUser_status']")
WebElement StatusinAdd;
@FindBy(xpath="//input[@id='systemUser_password']")
WebElement PasswordinAdd;
@FindBy(xpath="//input[@id='systemUser_confirmPassword']")
WebElement ConfirmPasswordinAdd;
@FindBy(xpath="//input[@id='btnAdd']")
WebElement Add;
@FindBy(xpath="//input[@id='btnSave']")
WebElement Save;
@FindBy(xpath="//a[@id='menu_admin_Job']")
WebElement Job;
@FindBy(xpath="//a[@id='menu_admin_viewJobTitleList']")
WebElement JobTitle;
@FindBy(xpath="//a[contains(.,'Job Title')][1]")
WebElement JobTitleone;
@FindBy(xpath="//span[contains(.,'Job Description')]")
WebElement JobDesciption;
@FindBy(xpath="//td[contains(.,'Anindita35126')]")
WebElement AddValidation;
@FindBy(xpath="//input[@id='searchSystemUser_employeeName_empName']")
WebElement searchEmployee;
@FindBy(xpath="//input[@value='9']")
WebElement DeleteButton;
@FindBy(xpath="//input[@id='btnDelete']")
WebElement btndelete;
@FindBy(xpath="//input[@id='dialogDeleteBtn']")
WebElement deleteconfirm;
public void logintoOrangeHrmsite() throws IOException{
	UserNameButton.sendKeys(cm.property_file_reading("Username"));
	PasswordButton.sendKeys(cm.property_file_reading("Password"));
	LoginButton.click();
	PropertyConfigurator.configure("Log4j.properties");
	logger.info("MethodName: logintoOrangeHrmsite >  has been executed successfully");
	
	
}
public boolean verifyvalidlogin(){
	String ExpectedTitle="OrangeHRM";
	String ActualTitle= driver.getTitle();
	if(ExpectedTitle.equalsIgnoreCase(ActualTitle)){
	PropertyConfigurator.configure("Log4j.properties");	
	logger.info("MethodName: verifyvalidlogin >  has been executed successfully");
	return true;
	}
	else{

	return false;
	}
	
	
}
public void logintoOrangeHrmsiteWithInvalidCredentials() throws IOException{
	UserNameButton.sendKeys(cm.property_file_reading("UsernameInvalid"));
	PasswordButton.sendKeys(cm.property_file_reading("PasswordInvalid"));
	LoginButton.click();
	if
	(InvalidCredentialsLoginMessage.isDisplayed())
	{
		System.out.println("Test case passed");
		
		
		}
	else
	{
  System.out.println("Test case failed");
		
		}
	PropertyConfigurator.configure("Log4j.properties");	
	logger.info("MethodName: logintoOrangeHrmsiteWithInvalidCredentials >  has been executed successfully");
	
	}
public void loginwitoutcredentials(){
	LoginButton.click();
	if(InvalidCredentialsLoginMessage.isDisplayed()){
		System.out.println("Testcase is passd");
	}
	else
	{
		System.out.println("Eroor occured");
	}
	PropertyConfigurator.configure("Log4j.properties");	
	logger.info("MethodName: loginwitoutcredentials >  has been executed successfully");
}

public void AdminModuleLaunch(){
	Admin.click();
	if(UserName.isEnabled())
			{
		SearchButton.isEnabled();
		System.out.println("Serchbutton is present");
		ResetButton.isEnabled();
		System.out.println("Resetbutton is present");
		UserType.isEnabled();
		System.out.println("Usertypebutton is present");
		EmployeeName.isEnabled();
		System.out.println("EmployeeNamebutton is present");
		Status.isEnabled();
		System.out.println("StatusButton is present");
		System.out.println(" Admin module launched successfully");
		}
	else{
		System.out.println("Error occured");
		}
	PropertyConfigurator.configure("Log4j.properties");	
	logger.info("MethodName: AdminModuleLaunch >  has been executed successfully");
        }
public void addFunctionality() throws Exception{
	Admin.click();
	Add.click();
	cm.SelectionMethod(UserRoleDropdown, "Admin");
	//ec.eLaunch("./TestData/TestData.xls", 0);
	//EmployeeNameinAdd.sendKeys(ec.eRead(0, 1));
	EmployeeNameinAdd.sendKeys(cm.property_file_reading("EmployeeName"));
	//UserNameinAdd.sendKeys(ec.eRead(1, 1));
	UserNameinAdd.sendKeys(cm.property_file_reading("UserNameinAdd"));
	cm.SelectionMethod(StatusinAdd, "Enabled");
	//PasswordinAdd.sendKeys(ec.eRead(2, 1));
	PasswordinAdd.sendKeys(cm.property_file_reading("PasswordinAdd"));
	//ConfirmPasswordinAdd.sendKeys(ec.eRead(3, 1));
	ConfirmPasswordinAdd.sendKeys(cm.property_file_reading("ConfirmPasswordinAdd"));
	CommonMethod.waitTill(3000);
	Save.click();
	CommonMethod.waitTill(3000);
	searchEmployee.sendKeys(cm.property_file_reading("searchEmployee"));
	CommonMethod.waitTill(3000);
	SearchButton.click();
	CommonMethod.waitTill(3000);
	if(AddValidation.isDisplayed()){
		System.out.println("User Added successfully");
	}
	else{
		System.out.println("Error Occured");
	}
	PropertyConfigurator.configure("Log4j.properties");	
	logger.info("MethodName: addFunctionality >  has been executed successfully");
        
	}
public void deletefunctionality(){
	Admin.click();
	CommonMethod.waitTill(3000);
	js = (JavascriptExecutor) driver;
	js.executeScript("window.scrollBy(0, 1900);");
	DeleteButton.click();
	js.executeScript("window.scrollBy(0, -1900);");
	btndelete.click();
	deleteconfirm.click();
}
public void jobTitleModulelaunch(){
CommonMethod.waitTill(3000);
Actions act=new Actions(driver);
act.moveToElement(Admin).build().perform();
CommonMethod.waitTill(3000);
act.moveToElement(Job).build().perform();
//cm.Mouse_Event(Admin);
CommonMethod.waitTill(3000);
//cm.Mouse_Event(Job);
JobTitle.click();
if(JobTitleone.isDisplayed()){
JobDesciption.isDisplayed();
System.out.println("User navigated to job Titles page successfully");
	}
else{
	System.out.println("Error Occured");
}
PropertyConfigurator.configure("Log4j.properties");	
logger.info("MethodName: jobTitleModulelaunch >  has been executed successfully");
}

        }

