package com.OrangeHrm.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CommonMethod {
	WebDriver driver;
	public String property_file_reading(String keyrequired) throws IOException
	{
		File file = new File("./TestData/Data.properties");
		FileInputStream fi = new FileInputStream(file);
		Properties prop = new Properties();
		prop.load(fi);
		return prop.getProperty(keyrequired);
		
	}

public void Mouse_Event(WebElement xyz){
	Actions act=new Actions(driver);
	act.moveToElement(xyz).build().perform();
}

public void SelectionMethod(WebElement Element,String xyz){
	Select Sel=new Select(Element);
	Sel.selectByVisibleText(xyz);
}
public void SelectionMethodbyindex(WebElement Element,int index){
	Select Sel=new Select(Element);
	Sel.selectByIndex(index);
}
public static void waitForElement(WebElement element, int waitTime, WebDriver driver) {
	
	WebDriverWait wait = new WebDriverWait(driver, waitTime);
	wait.until(ExpectedConditions.elementToBeClickable(element));
}

public static void waitTill(int waitTime) {
	try {
		Thread.sleep(waitTime);
	}
	catch(Exception e){
		System.out.println(e);
	}
}


}