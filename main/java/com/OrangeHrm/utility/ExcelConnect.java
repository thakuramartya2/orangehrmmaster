package com.OrangeHrm.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelConnect {
File f;
FileInputStream fs;
XSSFWorkbook wb;
XSSFSheet sheet;
String file_path;
public void eLaunch(String file_path, int sheet_index) throws Exception {
this.file_path=file_path;
f = new File (file_path);
fs= new FileInputStream(f);
wb = new XSSFWorkbook(fs);
sheet = wb.getSheetAt(sheet_index);
}
public String eRead(int row, int column) {
String s = sheet.getRow(row).getCell(column).getStringCellValue();
return s;
 }
public String eReadNumeric(int row, int column) {
double s = sheet.getRow(row).getCell(column).getNumericCellValue();
int i = (int) s;
String s1=""+i;
return s1;
}
 public void eWrite(int row, int column,String str) throws IOException
{
 FileOutputStream fout = new FileOutputStream(f);
sheet.getRow(row).createCell(column).setCellValue(str);
wb.write(fout);
}
}




