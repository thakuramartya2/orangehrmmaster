package com.OrangeHrm.test;

import java.io.IOException;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.OrangeHrm.pages.LoginPage;
import com.OrangeHrm.utility.Browserfactory;
import com.OrangeHrm.utility.CommonMethod;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LoginTestScript {
    Browserfactory browserfactory;
    LoginPage loginPage;
	WebDriver driver;
	ExtentHtmlReporter htmlReporter;
	ExtentReports extent;
	ExtentTest test;

	@BeforeMethod
	public void BrowserAppLaunch() throws InterruptedException, IOException 
	{htmlReporter = new ExtentHtmlReporter("./reports/extent.html");
	extent = new ExtentReports();
	extent.attachReporter(htmlReporter);
	CommonMethod cm=new CommonMethod();
	driver=Browserfactory.StartBrowser(cm.property_file_reading("Browser"), cm.property_file_reading("OrangeHrmurl"));
	Thread.sleep(5000);
	}

	@Test(priority = 0, enabled=false, description="TC_001: Verifying valid login functionality")
	public void verifyValidLogin() throws IOException {
		test = extent.createTest("Verifying valid login functionality");
		loginPage = PageFactory.initElements(driver, LoginPage.class);
		loginPage.logintoOrangeHrmsite();
		Assert.assertEquals(true, loginPage.verifyvalidlogin());
	
		
	}
	@Test(priority = 1, enabled=false, description="TC_002: Verifying Invalid login functionality")
	public void verifyInvalidLogin() throws IOException{
		test = extent.createTest("Verifying Invalid login functionality");
		loginPage = PageFactory.initElements(driver, LoginPage.class);
		loginPage.logintoOrangeHrmsiteWithInvalidCredentials();
		
	}
	@Test(priority = 2, enabled=false, description="TC_003: Verifying login functionality without credentials")
	public void verifyLoginWithoutCredentials() throws IOException{
		test = extent.createTest("Verifying login functionality without credentials");
		loginPage = PageFactory.initElements(driver, LoginPage.class);
		loginPage.loginwitoutcredentials();
		
	}
	@Test(priority = 3, enabled=false, description="TC_004: Verifying Admin Module launch functionality")
	public void verifyAdmin() throws IOException{
		test = extent.createTest("Verifying Admin Module launch functionality");
		loginPage = PageFactory.initElements(driver, LoginPage.class);
		loginPage.logintoOrangeHrmsite();
		loginPage.AdminModuleLaunch();
	}
	@Test(priority = 4, enabled=false, description="TC_005: Verifying Add functionality")
	public void verifyAdd() throws Exception{
		test = extent.createTest("Verifying Add functionality");
		loginPage = PageFactory.initElements(driver, LoginPage.class);
		loginPage.logintoOrangeHrmsite();
		loginPage.addFunctionality();
		}
	@Test(priority = 5, enabled=true, description="TC_006: Navigation of job title functionality")
	public void verifyJob() throws Exception{
		test = extent.createTest("Verifying Navigation of job title functionality");
		loginPage = PageFactory.initElements(driver, LoginPage.class);
		loginPage.logintoOrangeHrmsite();
		loginPage.jobTitleModulelaunch();
		}
	@Test(priority = 6, enabled=false, description="TC_007: Verifying delete functionality")
	public void deletefunctionality() throws Exception{
		test = extent.createTest("Verifying delete functionality");
		loginPage = PageFactory.initElements(driver, LoginPage.class);
		loginPage.logintoOrangeHrmsite();
		loginPage.deletefunctionality();
		}
	@AfterMethod
	public void closeApp(ITestResult testResults) {
		driver.quit();
		if(testResults.getStatus()==ITestResult.FAILURE) {
			test.log(Status.FAIL, "The failure exception below");
			test.log(Status.FAIL, testResults.getThrowable());
		}
		else if(testResults.getStatus()==ITestResult.SUCCESS) {
			test.log(Status.PASS, "The Test case is passed");
		}
		else if(testResults.getStatus()==ITestResult.SKIP) {
			test.log(Status.SKIP, testResults.getThrowable());
		}
		
		extent.flush();
	}
	
	@AfterClass
	public void reportWriter()
	{
		//extent.flush();
	}
}
